﻿using System.Collections.Generic;

namespace DiDrDe.Functional.Funcs
{
    public class SmartClass
    {
        public IEnumerable<string> ConvertToListOfUpperCaseWords(string sentence)
        {
            return sentence.ToUpper().Split(' ');
        }
    }
}
